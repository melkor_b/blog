import isEqual from 'lodash/fp/isEqual';
import curry from 'lodash/fp/curry';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const notEqual = curry((a: any): any => !isEqual(a));
